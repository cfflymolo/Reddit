//
//  EndpointController.swift
//  IEX
//
//  Created by Robert Colin on 3/4/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

class EndpointController {
  
  enum HTTPMethod: String {
    case GET
    case PATCH
    case POST
    case PUT
    case DELETE
  }
  
  typealias JSON = [String: Any]
  
  fileprivate let urlHost: String
  fileprivate let processingQueue: OperationQueue
  
  // MARK: - Initialization
  
  init(withUrlHost host: String, processingQueue queue: OperationQueue) {
    processingQueue = queue
    urlHost = host
  }
  
  convenience init(withUrlHost host: String) {
    let queue: OperationQueue = OperationQueue()
    queue.maxConcurrentOperationCount = 1
    
    self.init(withUrlHost: host, processingQueue: queue)
  }
  
  // MARK: - Internal
  
  func fetch<A: Decodable>(forEndpoint endpoint: String, urlParameters: [String: Any]? = nil, completion: @escaping (A?, Error?) -> ()) {
    
    guard let request = urlRequest(forEndpoint: endpoint, urlParameters: urlParameters) else {
      completion(nil, EndpointError.invalidUrl(host: urlHost, endpoint: endpoint, parameters: urlParameters))
      return
    }
    
    fetch(withRequest: request, completion: completion)
  }
  
  func fetch<A: Decodable>(withRequest request: URLRequest, completion: @escaping (A?, Error?) -> ()) {
    
    let dataTask = URLSession.shared.dataTask(with: request) { data, response, error in
      
      if let error = error {
        OperationQueue.main.addOperation {
          completion(nil, error)
        }
        
        return
      }
      
      guard let data = data, let decoded = try? JSONDecoder().decode(A.self, from: data) else {
        OperationQueue.main.addOperation {
          completion(nil, EndpointError.invalidJSONResponse(forExpectedType: A.self))
        }
        
        return
      }
      
      OperationQueue.main.addOperation {
        completion(decoded, nil)
      }
    }
    
    processingQueue.addOperation(dataTask.resume)
  }
  
  func fetchSingleValue<A: Decodable>(forEndpoint endpoint: String, key: String, completion: @escaping (A?, Error?) -> ()) {
    
    guard let request = urlRequest(forEndpoint: endpoint) else {
      completion(nil, EndpointError.invalidUrl(host: urlHost, endpoint: endpoint, parameters: nil))
      return
    }
    
    let dataTask = URLSession.shared.dataTask(with: request) { data, response, error in
      
      if let error = error {
        OperationQueue.main.addOperation {
          completion(nil, error)
        }
        
        return
      }
      
      guard
        let data = data,
        let json = try? JSONSerialization.jsonObject(with: data, options: []) as? JSON,
        let value = json?[key] as? A else {
          
          OperationQueue.main.addOperation {
            completion(nil, EndpointError.missingJSONKey(key, forType: A.self))
          }
          
          return
      }
      
      OperationQueue.main.addOperation {
        completion(value, nil)
      }
    }
    
    processingQueue.addOperation(dataTask.resume)
  }
  
  // MARK: - File Private
  
  fileprivate func urlRequest(forEndpoint endpoint: String, httpMethod: HTTPMethod = .GET, bodyParameters: [String: Any]? = nil, urlParameters: [String: Any]? = nil) -> URLRequest? {
    
    var components: URLComponents = URLComponents()
    components.scheme = "https"
    components.host = urlHost
    components.path = endpoint
    components.queryItems = urlParameters?.map { URLQueryItem(name: $0, value: String(describing: $1)) }
    
    guard let url = components.url else {
      return nil
    }
    
    var request: URLRequest = URLRequest(url: url)
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = httpMethod.rawValue
    
    if let bodyParameters = bodyParameters {
      request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters, options: [])
    }
    
    return request
  }
}
