//
//  Comment.swift
//  Reddit
//
//  Created by Robert Colin on 4/14/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct Comment: Decodable {
  let authorName: String
  let body: String
  let identifier: String
  let kind: ObjectType
  
  enum CodingKeys: String, CodingKey {
    case authorName = "author"
    case body
    case data
    case identifier = "id"
    case kind
  }
}

extension Comment {
  var fullName: String {
    return "\(kind.rawValue)_\(identifier)"
  }
}

extension Comment {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let kindString = try container.decode(String.self, forKey: .kind)
    if let kindType = ObjectType(rawValue: kindString) {
      kind = kindType
    } else {
      throw DecodingError.dataCorruptedError(
        forKey: .kind,
        in: container,
        debugDescription: "Kind type does not match the format expected by the type.")
    }
    
    let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
    
    authorName = try data.decode(String.self, forKey: .authorName)
    body = try data.decode(String.self, forKey: .body)
    identifier = try data.decode(String.self, forKey: .identifier)
  }
}
