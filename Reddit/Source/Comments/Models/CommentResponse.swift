//
//  CommentResponse.swift
//  Reddit
//
//  Created by Robert Colin on 4/14/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct CommentResponse: Decodable {
  let comments: [Comment]
  
  enum CodingKeys: String, CodingKey {
    case children
    case data
  }
}

extension CommentResponse {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
    
    comments = try data.decode([Comment].self, forKey: .children)
  }
}
