//
//  Image.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import UIKit

struct Image: Decodable {
  let size: CGSize
  let url: URL
  
  enum CodingKeys: String, CodingKey {
    case height
    case width
    case url
  }
}

extension Image {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let heightValue = try container.decode(Int.self, forKey: .height)
    let widthValue = try container.decode(Int.self, forKey: .width)
    
    size = CGSize(width: widthValue, height: heightValue)
    url = try container.decode(URL.self, forKey: .url)
  }
}
