//
//  LinkController.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct LinkController {
  let endpointController: EndpointController
  
  init() {
    endpointController = EndpointController(withUrlHost: "www.reddit.com")
  }
  
  func fetchComments(forSubreddit subreddit: String, articleIdentifier identifier: String, limit: Int = 25, completion: @escaping (LinkResponse?, Error?) -> ()) {
    
    endpointController.fetch(
      forEndpoint: "/r/\(subreddit)/comments/\(identifier).json",
      urlParameters: ["depth": 1, "limit": limit],
      completion: completion
    )
  }
  
  func fetchAllLinks(limit: Int = 25, completion: @escaping (LinkResponse?, Error?) -> ()) {
    
    endpointController.fetch(forEndpoint: "/r/all.json", urlParameters: ["limit": limit], completion: completion)
  }
  
  func fetchLinks(limit: Int = 25, after: String, completion: @escaping (LinkResponse?, Error?) -> ()) {
    
    endpointController.fetch(forEndpoint: "/r/all.json", urlParameters: ["after": after, "limit": limit], completion: completion)
  }
}
