//
//  LinksViewModel.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

final class LinksViewModel {
  static let notificationName: Notification.Name = Notification.Name(rawValue: "LinksViewModel.NewLinks")
  
  let linkController: LinkController = LinkController()
  var links: [Link] = []
  var lastIdentifier: String?
  
  func fetchPosts(after: String? = nil) {
    linkController.fetchAllLinks { [weak self] response, error in
      if let error = error {
        print(error)
      }
      
      if let response = response {
        self?.lastIdentifier = response.links.last?.fullName
        self?.links += response.links
        
        NotificationCenter.default.post(Notification(name: LinksViewModel.notificationName))
      }
    }
  }
  
  func fetchNextPage() {
    guard let lastIdentifier = lastIdentifier else { return }
    
    linkController.fetchLinks(after: lastIdentifier) { [weak self] response, error in
      if let error = error {
        print(error)
      }
      
      if let response = response {
        self?.lastIdentifier = response.links.last?.fullName
        self?.links += response.links
        
        NotificationCenter.default.post(Notification(name: LinksViewModel.notificationName))
      }
    }
  }
}
