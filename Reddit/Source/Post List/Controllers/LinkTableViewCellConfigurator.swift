//
//  LinkTableViewCellConfigurator.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct LinkTableViewCellConfigurator {
  func configure(_ cell: LinkTableViewCell, forDisplaying link: Link) {
    cell.titleLabel?.text = link.title
    cell.authorLabel?.text = link.authorName
    
    if let _ = link.thumbnail {
      cell.thumbnailImageView?.isHidden = false
    } else {
      cell.thumbnailImageView?.isHidden = true
    }
  }
}
