//
//  Link.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct Link: Decodable {
  let authorName: String
  let identifier: String
  let kind: ObjectType
  let title: String
  let thumbnail: URL?
  let url: URL?
  
  enum CodingKeys: String, CodingKey {
    case authorName = "author"
    case data
    case identifier = "id"
    case kind
    case title
    case thumbnail
    case url
  }
}

extension Link {
  var fullName: String {
    return "\(kind.rawValue)_\(identifier)"
  }
}

extension Link {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let kindString = try container.decode(String.self, forKey: .kind)
    if let kindType = ObjectType(rawValue: kindString) {
      kind = kindType
    } else {
      throw DecodingError.dataCorruptedError(
        forKey: .kind,
        in: container,
        debugDescription: "Kind type does not match the format expected by the type.")
    }
    
    let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
    
    authorName = try data.decode(String.self, forKey: .authorName)
    identifier = try data.decode(String.self, forKey: .identifier)
    title = try data.decode(String.self, forKey: .title)
    thumbnail = try data.decode(URL?.self, forKey: .thumbnail)
    url = try data.decode(URL?.self, forKey: .url)
  }
}
