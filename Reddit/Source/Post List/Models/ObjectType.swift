//
//  ObjectType.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

enum ObjectType: String {
  case comment = "t1"
  case account = "t2"
  case link = "t3"
  case message = "t4"
  case subreddit = "t5"
  case award = "t6"
}
