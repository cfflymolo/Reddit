//
//  LinkResponse.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

struct LinkResponse: Decodable {
  let after: String?
  let before: String?
  let links: [Link]
  
  enum CodingKeys: String, CodingKey {
    case after
    case before
    case children
    case data
  }
}

extension LinkResponse {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
    
    after = try data.decode(String?.self, forKey: .after)
    before = try data.decode(String?.self, forKey: .before)
    links = try data.decode([Link].self, forKey: .children)
  }
}
