//
//  LinkTableViewCell.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {
  static let identifier: String = "LinkTableViewCell"
  
  var titleLabel: UILabel?
  var authorLabel: UILabel?
  var thumbnailImageView: UIImageView?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    let baseStackView: UIStackView = UIStackView()
    baseStackView.axis = .horizontal
    
    let middleStackView: UIStackView = UIStackView()
    middleStackView.axis = .vertical
    middleStackView.spacing = 8
    
    let topStackView: UIStackView = UIStackView()
    topStackView.axis = .horizontal
    topStackView.spacing = 8
    
    let authorLabel: UILabel = UILabel()
    authorLabel.font = UIFont.systemFont(ofSize: 12)
    authorLabel.textColor = .black
    authorLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .vertical)
    self.authorLabel = authorLabel
    
    let titleLabel: UILabel = UILabel()
    titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
    titleLabel.numberOfLines = 2
    titleLabel.textColor = .black
    titleLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 750), for: .horizontal)
    titleLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .vertical)
    
    titleLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
    titleLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
    self.titleLabel = titleLabel
    
    let imageView: UIImageView = UIImageView()
    imageView.contentMode = .scaleAspectFill
    imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.0/1.0).isActive = true
    imageView.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
    imageView.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
    self.thumbnailImageView = imageView
    
    addSubview(baseStackView)
    baseStackView.translatesAutoresizingMaskIntoConstraints = false
    
    baseStackView.addArrangedSubview(middleStackView)
    middleStackView.addArrangedSubview(topStackView)
    middleStackView.addArrangedSubview(authorLabel)
    topStackView.addArrangedSubview(titleLabel)
    topStackView.addArrangedSubview(imageView)
    
    NSLayoutConstraint.activate([
      baseStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
      baseStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
      baseStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
      baseStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor)
    ])
  }
}
