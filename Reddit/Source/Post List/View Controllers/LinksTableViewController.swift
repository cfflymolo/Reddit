//
//  LinksTableViewController.swift
//  Reddit
//
//  Created by Robert Colin on 4/13/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import UIKit

class LinksTableViewController: UITableViewController {
  
  // MARK: - Properties
  
  let configurator: LinkTableViewCellConfigurator = LinkTableViewCellConfigurator()
  var linksViewModel: LinksViewModel = LinksViewModel()
  
  // MARK: - View Lifecycle
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(forName: LinksViewModel.notificationName, object: nil, queue: .main) { [unowned self] _ in
      self.tableView.reloadData()
    }
    
    linksViewModel.fetchPosts()
  }
}

// MARK: - UITableViewDataSource

extension LinksTableViewController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return linksViewModel.links.count
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: LinkTableViewCell.identifier) else {
      fatalError("No table view cell was dequeued at indexPath: \(indexPath)")
    }
    
    let link: Link = linksViewModel.links[indexPath.row]
    if let linksTableViewCell = tableViewCell as? LinkTableViewCell {
      configurator.configure(linksTableViewCell, forDisplaying: link)
    }
    
    return tableViewCell
  }
  
  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if indexPath.row == linksViewModel.links.count - 1 {
      linksViewModel.fetchNextPage()
    }
  }
}

// MARK: - UITableViewDelegate

extension LinksTableViewController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    navigationController?.pushViewController(UIViewController(), animated: true)
  }
  
  override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 120
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}
